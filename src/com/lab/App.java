package com.lab;
import java.util.Random;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.atomic.AtomicReference;

public class App extends JFrame {
    private JButton CLICK_ME;
    private JPanel mainForm;
    static    JFrame frame = new JFrame("App" );


    public App(int xw,int yw) {
        frame.setUndecorated(true);
        CLICK_ME.addActionListener(new ActionListener() {
            int x=xw;
            int y=yw;
           @Override
            public void actionPerformed(ActionEvent e) {
                Random rand = new Random();


                 CLICK_ME.setSize(200,200);
                int x1= rand.nextInt(200);
                int y1=rand.nextInt(200);
                while( x != x1 && y!= y1)
                {
                    if(x >x1)
                    {
                        --x;

                    }
                    else if(x<x1)
                    {
                        ++x;


                    }
                    if(y >y1)
                    {
                        --y;

                    }
                    else if(y<y1)
                    {
                        ++y;


                    }
                    try {
                        Thread.currentThread().sleep(10);
                    } catch (InterruptedException ie) {

                    }
                    frame.setLocation(x, y);
                    frame.pack();


                }

            }
        });

    }

    public static void main(String[] args) throws InterruptedException {
        Random rand = new Random();
        int x = rand.nextInt(1000);
        int y= rand.nextInt(800);

        frame.setContentPane(new App(x,y).mainForm);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocation(x,y);
        frame.setVisible(true);


    }



}
